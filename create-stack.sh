#!/bin/bash
cd openstack-heat-slurm
set -x
openstack stack create -t slurm.yaml    \
			-e lib/env.yaml \
			--parameter "count=4;flavor=c1.large;key_name=fgaudet-key;image_id=7c09f5aa-52fa-4daf-a14d-8956cde1e0f2;net_id=fgaudet-net2;name=fgaudet-slurm" \
			slurm-stack