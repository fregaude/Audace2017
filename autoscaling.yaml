heat_template_version: 2014-10-16

description: >
  This is a very simple template that illustrates automatic scaling up and down
  using OS::Heat::AutoScalingGroup. CPU load is generated for
  3 mins after each instance is spawned, triggering a scale-up event.
  Once the max number of instances has been reached and the CPU
  load generation has finished, the number of instances will be scaled
  back down to 1.

parameters:
  key_name:
    type: string
    description: Keypair for authentication
    default: fgaudet-key
  flavor:
    type: string
    description: Flavor of the servers
    default: 2
  image:
    type: string
    description: Image used to boot the servers
    default: 05b6cf31-8825-4e1e-9140-e352df0a52d9
  name:
    type: string
    description: Name of each machine booted
    default: ASG-Audace2017
  net_privateid:
    type: string
    label: Network ID
    description: ID of the private network to use
    default: e64aadec-aa13-4985-aadd-1044b4372192

resources:
  cloud_config_stress:
    type: OS::Heat::CloudConfig
    properties:
      cloud_config:
        package_update: true
        packages:
          - stress
        runcmd:
          - stress --cpu 1 --timeout 3m

  asg:
    type: OS::Heat::AutoScalingGroup
    properties:
      min_size: 1
      max_size: 10
      resource:
        type: OS::Nova::Server
        properties:
          key_name: {get_param: key_name}
          flavor: {get_param: flavor}
          image: {get_param: image}
          networks:
            - network: { get_param: net_privateid }
          metadata: {"metering.stack": {get_param: "OS::stack_id"}}
          user_data_format: RAW
          user_data:
            get_resource: cloud_config_stress
          name: { get_param: name }

  scale_up_policy:
    type: OS::Heat::ScalingPolicy
    properties:
      adjustment_type: change_in_capacity
      auto_scaling_group_id: {get_resource: asg}
      cooldown: 60
      scaling_adjustment: 1

  scale_down_policy:
    type: OS::Heat::ScalingPolicy
    properties:
      adjustment_type: change_in_capacity
      auto_scaling_group_id: {get_resource: asg}
      cooldown: 60
      scaling_adjustment: '-1'

  cpu_alarm_high:
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-up if the average CPU > 50% for 1 minute
      meter_name: cpu_util
      statistic: avg
      period: 60
      evaluation_periods: 1
      threshold: 50
      alarm_actions:
        - {get_attr: [scale_up_policy, alarm_url]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: gt

  cpu_alarm_low:
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-down if the average CPU < 15% for 2 minutes
      meter_name: cpu_util
      statistic: avg
      period: 120
      evaluation_periods: 1
      threshold: 15
      alarm_actions:
        - {get_attr: [scale_down_policy, alarm_url]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: lt

outputs:
  server_list:
    description: >
      List of server names that are part of the group.
    value: {get_attr: [asg, outputs_list, name]}