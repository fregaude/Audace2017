 #/bin/bash
set -v
FLAVOR=m1.small
IMAGE="Ubuntu Server 16.04 LTS (xenial)"
NET_ID=e64aadec-aa13-4985-aadd-1044b4372192
KEY=fgaudet-key

openstack server create --flavor $FLAVOR --image "$IMAGE" --key $KEY --nic net-id=$NET_ID audace1