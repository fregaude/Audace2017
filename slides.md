%title: Presentation Audace 2017
%author: Frédéric Gaudet
%date: 2017-06-01

-> Who Am I <-
=========

Frédéric Gaudet

Ingénieur de recherche CNRS/LIMOS

-------------------------------------------------

-> Overview <-
=========

>Galactica introduction
>>What is it ?
>>How to apply
>How does it works

-------------------------------------------------

-> Galactica <-
=========

*Experiments oriented*

Users can easily access :

>Datasets
>Already configured machines
>Statistics
>Web access

-------------------------------------------------


-> OpenStack <-
=========

*Resource orchestrator : IaaS*

> Compute
> Storage (Ceph backend)
> Network

-------------------------------------------------

-> Key Figures <-
=========

>180 vCPU
>2 TB RAM
>120 TB raw storage
>10G Network

-------------------------------------------------

-> How to use it ? <-
=========

*It's an Open platform*

>Send us an email 
>>(gaudet@isima.fr or ftoumani@isima.fr)
>Write a « fiche d'expérimentation »
>Use the resources
>Then stash them :)

Don't forget the last one ...

-------------------------------------------------

-> Fiche d'expérimentation <-
=========

>Project objectives
>Resources needed (CPU, RAM, software)
>Datasets
>Expected results

# https://galactica.isima.fr/works

-------------------------------------------------

-> Create a VM <-
=========

>Flavor
>Image
>>2 differents ways
>>Use a vanilla image -> deploy, install, 
>>configure and run your software
>>Create an image -> deploy, 
>>configure and run your software
>Keypair
>Network

-------------------------------------------------

-> Load some data <-
=========

>From your servers
>>SCP, Rsync

-------------------------------------------------

-> Load some data <-
=========

>From the Object Store

    openstack --os-tenant-name petasky \
    --os-region-name RegionOne \
    --os-username fred  \
    --os-password ******** \
    --os-auth-url \
    https://auth.oscloud.isima.fr/v2.0 \
    object save lsst ObjectSource_456.csv

-------------------------------------------------

-> Orchestration <-
=========

*Goal : create a bunch of servers*

>Stack
>Easy management

-------------------------------------------------

-> Create a stack <-
=========

*So far we've got several stack definition*

*Provided by OpenStack Plugin*
>Hadoop, Spark

*Homemade*
>Cassandra, OpenMPI, Slurm

-------------------------------------------------

-> Get statistics <-
=========

*Many metrics available :*

>RAM usage
>CPU usage
>disk read/write
>network read/write

-------------------------------------------------

-> Auto Scaling Group <-
=========

*A little demo, just for fun*

-------------------------------------------------

-> Conclusion <-
=========

*Galactica : versatile toolbox*

>Used for experiences
>>Qserv, TriAnnot, emirge/reago, Spark ...

>Used for services
>>Gitlab CI, Web services, Web sites
>>FlexLM ...

-------------------------------------------------

-> Thanks <-
=========

All resources you've just seen are available there :

# https://gitlab.isima.fr/fgaudet/Audace2017

Any questions ?
